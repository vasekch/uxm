# UXM #

User Experience Monitoring

# Generate E-R diagram:
./manage.py graph_models -agE  cp | dot -Tjpg  -o ../docs/er_diagram.jpg

# How to run celery?
./manage.py celery -A tasks worker
