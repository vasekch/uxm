.. developent_workflows:

Develop
=======

Develop information and instruction

Generate documentation
----------------------

to generate new version of documentation::

	$ cd docs
	$ make html

open your browser on ``file:///<your_path_to_project>/doc/build/html/index.html``


Vizual database structure
-------------------------

Make sure you have 'dot' tool available. 'sudo apt-get install graphviz'

    source env/bin/activate
    cd src
    ./manage.py graph_models -agE | dot -Tpng  -o ../doc/er_diagram.png

.. image:: ./er_diagram.png
