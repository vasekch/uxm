.. development_install:

Development Installation
========================


Database
^^^^^^^^

Clean postgreSQL installation

	$ sudo apt-get install postgresql
	$ sudo su - postgres
	$ psql
	postgres=# CREATE USER uxm WITH PASSWORD 'uxm';
	postgres=# CREATE DATABASE uxm OWNER uxm;
	postgres=# DROP schema public
	postgres=# \q

Set password authentication for postgres in ``/etc/postgresql/9.3/main/pg_hba.conf`` change line::

	local   all             all                                     peer

to line::

	local   all             all                                     md5

connect as regular user and create schema::

	$ psql -U uxm -W -d uxm


UXM - how to install

Required packages
-----------------

install dependencies::

	sudo apt-get install libpq-dev
	sudo apt-get install python3-dev
	sudo apt-get install redis-server

pyvenv-3.4 env
--------------

Create virtual environment to separate project from system::

	pyvenv-3.4 env

# OR ubuntu 14.04 bug: https://bugs.launchpad.net/ubuntu/+source/python3.4/+bug/1290847::

	pyvenv-3.4 --without-pip env
	source ./env/bin/activate
	wget https://pypi.python.org/packages/source/s/setuptools/setuptools-7.0.tar.gz
	tar -vzxf setuptools-6.1.tar.gz
	cd setuptools-6.1
	python setup.py install
	cd ..
	wget https://pypi.python.org/packages/source/p/pip/pip-1.5.6.tar.gz
	tar -vzxf pip-1.5.6.tar.gz
	cd pip-1.5.6
	python setup.py install
	cd ..
	deactivate
	source ./env/bin/activate

install requirements::

	pip install -r requirements.txt

configure
---------

edit ``settings/local.py`` and set your database credentials::

	cd src
	cp settings/local.py.template settings/local.py

prepare database::

	./manage.py migrate

create superuser if you don't have one::

	./manage.py createsuperuser

start server::

	./manage.py runserver

point your browser to::
	http://localhost:8000/

if you make some change in database models run::

	./manage.py makemigrations appname




Elasticsearch
^^^^^^^^^^^^^

Project dependencies and libraries
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
