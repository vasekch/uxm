.. server_install:

CentOS 6 Installation
=====================

CentOS 6 installation manual for production environment.

Database - PostgreSQL
^^^^^^^^^^^^^^^^^^^^^

Set password authentication for postgres in ``/var/lib/pgsql/9.3/data/pg_hba.conf`` change line::

	# TYPE  DATABASE        USER            ADDRESS                 METHOD
	# allow project connection
	host   uxmcp            uxmcp           			            md5
	# allow postgres user shell connection without pasword
	local   all             postgres                                     peer

restart postgres::

	/etc/init.d/postgresql-9.3 restart

	$ sudo su - postgres
	$ psql
	postgres=# CREATE USER uxmcp WITH PASSWORD '<passwd>';
	postgres=# CREATE DATABASE uxmcp OWNER uxmcp;
	postgres=# DROP schema public;
	postgres=# \q

connect as regular user and create schema::

	$ psql -h localhost -U uxmcp -W -d uxmcp
	uxmcp=# CREATE schema public;

Requirements
^^^^^^^^^^^^

install dependencies::

	yum install libpqxx-devel
	yum install python3-devel
	yum install redis28u

Project specifics
^^^^^^^^^^^^^^^^^

Checkout project
----------------

make OS user ``uxmcp``::

	TODO

switch to user uxmcp::

	su - uxmcp

create project dir::

	mkdir /opt/uxmcp
	cd /opt/uxmcp

check out using git::

	git clone https://bitbucket.org/coex/uxm.git

ALTERNATIVE TODO download tar.bz2 and unzip project to folder::

	https://own.rtdn.cz/public.php?service=files&t=5c8498847613f5bd7a23af3f9da17111
	TODO

pyvenv-3.4 env
--------------

Create virtual environment to separate project from system::

	cd /opt/uxmcp

	pyvenv env # python 3.3
    pyvenv --without-pip env # python 3.4
    source ./env/bin/activate

    wget https://pypi.python.org/packages/source/s/setuptools/setuptools-6.1.tar.gz
    tar -vzxf setuptools-6.1.tar.gz
    cd setuptools-6.1/
    python setup.py install
    cd ..

    wget https://pypi.python.org/packages/source/p/pip/pip-1.5.6.tar.gz
    tar -vzxf pip-1.5.6.tar.gz
    cd pip-1.5.6
    python setup.py install
    cd ..


    rm pip-1.5.6 setuptools-6.1 -rf
    rm pip-1.5.6.tar.gz setuptools-6.1.tar.gz

    deactivate
	source ./env/bin/activate

install requirements::

	pip install -r requirements.txt

configure
---------

change directory to src::

	cd src

create copy of settings template::

	cp settings/local.py.template settings/local.py

edit ``src/settings/local.py`` and set your credentials for database, elasticsearch, redis, etc. see all defaults ins ``src/settings/base.py`` and don't forget to add path to static media used by collectstatic::

	STATIC_ROOT = '/opt/uxmcp/static'

prepare database structure::

	./manage.py migrate

prepare static media to /opt/uxmcp/static ::

	./manage.py collectstatic

[optional] create superuser if you don't have one in your inital data or dump::

	./manage.py createsuperuser


Supervisord
^^^^^^^^^^^

Install supervisor as root to system, not to virtualenv created for project::

	pip install supervisord

TODO JC - kde se vzali init scripty

Create /etc/supervisor/supervisord.conf::

	[unix_http_server]
	file = /var/run/supervisor.sock   ; (the path to the socket file)
	chmod = 0700                       ; sockef file mode (default 0700)
	chown = uxmcp:uxmcp

	[supervisord]
	logfile=/var/log/supervisor/supervisord.log ; (main log file;default $CWD/supervisord.log)
	pidfile=/var/run/supervisord.pid ; (supervisord pidfile;default supervisord.pid)
	childlogdir=/var/log/supervisor  ; ('AUTO' child log dir, default $TEMP)
	environment=LANG="en_US.utf8",LC_ALL="en_US.UTF-8",LC_LANG="en_US.UTF-8"

	; the below section must remain in the config file for RPC
	; (supervisorctl/web interface) to work, additional interfaces may be
	; added by defining them in separate rpcinterface: sections

	[rpcinterface:supervisor]
	supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

	[supervisorctl]
	serverurl = unix:///var/run/supervisor.sock ; use a unix:// URL  for a unix socket

	; The [include] section can just contain the "files" setting.  This
	; setting can list multiple files (separated by whitespace or
	; newlines).  It can also contain wildcards.  The filenames are
	; interpreted as relative to this file.  Included files cannot
	; include files themselves.

	[include]
	files = /etc/supervisor/conf.d/*.conf


Create /etc/supervisor/conf.d/uxmcp.conf::

	[program:uxmcp_gunicorn]
	command=/opt/uxmcp/env/bin/gunicorn --workers=4 --timeout=1800 --bind=unix:/opt/uxmcp/gunicorn.sock wsgi
	directory=/opt/uxmcp/src/
	user=uxmcp
	autostart=true
	autorestart=true
	redirect_stderr=true

	[program:uxmcp_celery]
	command=/opt/uxmcp/env/bin/python manage.py celeryd --concurrency=1 --loglevel=INFO
	directory=/opt/uxmcp/src/
	user=uxmcp
	numprocs=1
	stdout_logfile=/opt/uxmcp/logs/celeryd.log
	stderr_logfile=/opt/uxmcp/logs/celeryd.log
	autostart=true
	autorestart=true
	startsecs=3

	[group:uxmcp]
	programs=uxmcp_gunicorn,uxmcp_celery

Create symlink to standard path where supervisorctl expects configuration::

	ln -s /etc/supervisor/supervisord.conf /etc/

Restart supervisor service using supervisorctl command or standard way::

	/etc/init.d/supervisord restart

Check everything is ok in supervisorctl::

	supervisorctl
	supervisor> status

Nginx
^^^^^

Edit proxy settings in ``/etc/nginx/proxy.conf``::

	proxy_redirect     off;
	proxy_set_header   Host             $host;
	proxy_set_header   X-Real-IP        $remote_addr;
	proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
	proxy_max_temp_file_size 0;
	proxy_connect_timeout      90;
	proxy_send_timeout         90;
	proxy_read_timeout         90;
	proxy_buffer_size          4k;
	proxy_buffers              4 32k;
	proxy_busy_buffers_size    64k;
	proxy_temp_file_write_size 64k;

Create ``/etc/nginx/sites-available/uxm``::

	server {
	    listen 80;
	    server_name uxmcp.rtdn.cz;
	    access_log /opt/uxmcp/logs/nginx.access.log;
	    client_max_body_size 10m;
	    location / {
	        proxy_pass http://unix:/opt/uxmcp/gunicorn.sock;
	        include /etc/nginx/proxy.conf;
	    }

	    # ./manage.py collatestatic product
	    location /static/ {
	        expires 1m;
	        alias /opt/uxmcp/static/;
	    }

	    # user media
	    location /media/ {
	        expires 1m;
	        alias /opt/uxmcp/src/media/;
	    }
	}

Enable UXM control panel in nginx::

	ln -s /etc/nginx/sites-available/uxmcp /etc/nginx/sites-enabled/

Restart nginx configuration::

	/etc/init.d/nginx restart

Services auto start
^^^^^^^^^^^^^^^^^^^

Add services to be automatically started after reboot

	chkconfig nginx on
	chkconfig redis on
	chkconfig supervisord on

Set crons
^^^^^^^^^

TODO VCh
