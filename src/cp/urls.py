from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from cp import views as cp_views

################################################################################


urlpatterns = patterns('cp.views',
    url(r'^$', login_required(cp_views.IndexView.as_view()), name="index"),

    url(r'^select-customer/(?P<pk>\d+)/$',
        login_required(cp_views.SelectCustomerView.as_view()),
        name="select_customer"
    ),
    url(r'^toggle-deployedtest-enabled/(?P<pk>\d+)/$',
        login_required(cp_views.ToggleDeployedTestEnabledView.as_view()),
        name="toggle_deployedtest_enabled"
    ),
    url(r'^toggle-testsetup-enabled/(?P<pk>\d+)/$',
        login_required(cp_views.ToggleTestSetupEnabledView.as_view()),
        name="toggle_testsetup_enabled"
    ),
    url(r'^build-test-setup/(?P<pk>\d+)/$',
        login_required(cp_views.BuildTestSetupView.as_view()),
        name="build_test_setup"
    ),
    url(r'^generate-deployed-test/(?P<agent_pk>\d+)/$',
        login_required(cp_views.GenerateDeployedTestView.as_view()),
        name="generate_deployed_test"
    ),
     url(r'^push-deployed-test/(?P<agent_pk>\d+)/$',
        login_required(cp_views.PushDeployedTestView.as_view()),
        name="push_deployed_test"
    ),
    url(r'^push-deployed-test-status/(?P<agent_pk>\d+)/$',
        login_required(cp_views.PushDeployedTestStatusView.as_view()),
        name="push_deployed_test_status"
    ),
)
