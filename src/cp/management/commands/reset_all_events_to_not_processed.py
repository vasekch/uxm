from datetime import datetime, timezone
from dateutil import parser
from math import floor

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import ElasticsearchException

from django.core.management.base import BaseCommand, CommandError

from django.conf import settings

class Command(BaseCommand):
    args = '<int size=10>'
    help = 'Get all sakuli events from elasticearch and set uxm_processed to false WARNING: this command is only for Development!'

    def handle(self, *args, **options):

        size = 0
        # parse arguments
        if len(args) == 0:
            size = 10
        elif len(args) > 1:
            raise CommandError('Only one argument <int size> is allowed')
        else:
            try:
                size = int(args[0])
            except ValueError:
                raise CommandError('Argument size must be <int>')

        # establish connection to Elasticsearch
        try:
            es = Elasticsearch(settings.ES_CONNECTION)
        except ElasticsearchException as e:
            raise CommandError("ERROR when connecting to Elasticsearch: %s" % e)


        self.stdout.write("Sakuli Event Documents (size = %s):" % size)

        try:
            # get latest events
            result = es.search(index="uxm-raw", doc_type="sakuli-event", q="tags:sakuli-suite AND uxm_processed:T", fields="", sort="log_timestamp:desc", size=size)
            self.stdout.write("Total hits: %s" % result["hits"]["total"])
            for hit in result["hits"]["hits"]:
                # update each event
                # es.delete(index="uxm-raw", doc_type="sakuli-event", id=hit["_id"])
                es.update(index=hit["_index"], doc_type="sakuli-event", id=hit["_id"], body={ "doc": { "uxm_processed" : False }})
                self.stdout.write("doc id %s updated to uxm_processed = false" % hit["_id"])


        except ElasticsearchException as e:
            self.stdout.write("ERROR when getting data from Elasticsearch: %s" % e)
