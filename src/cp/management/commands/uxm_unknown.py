from datetime import datetime, timezone
from dateutil import parser
from math import floor

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import ElasticsearchException

from django.core.management.base import BaseCommand, CommandError

from cp.models import Customer
# from cp.models import DeployedTest

from django.conf import settings

class Command(BaseCommand):
    args = '<datetime> or <unknown_start> <unknown_end>'
    help = 'Generates unknown frame for provided ISO8601 datetime - single frame or period filled according to settings.UXM_FRAME_SIZE depending on number of parameters. Example usage: manage.py uxm_unknown 2014-12-15T10:00:00+0100 2014-12-15T18:00:00+0100'

    def handle(self, *args, **options):

        if len(args) not in (1, 2):
            raise CommandError('Please provide at least one datetime in ISO8601 format')

        try:
            unknown_start = parser.parse(args[0]).timestamp()
            if len(args) == 2:
                unknown_end = parser.parse(args[1]).timestamp()
            else:
                unknown_end = unknown_start
        except ValueError as e:
            raise CommandError("Args contains not valid timestamp %s" % e)

        if unknown_start >= unknown_end:
            raise CommandError("Unknown end must be later than unknown start")

        frames = list()
        time_pointer = unknown_start

        while time_pointer <= unknown_end:
            ts = floor(time_pointer / settings.UXM_FRAME_SIZE) * settings.UXM_FRAME_SIZE
            frames.append(datetime.fromtimestamp(ts))
            time_pointer = time_pointer + settings.UXM_FRAME_SIZE

        self.stdout.write("time frames ok: total %s" % len(frames))

        try:
            es = Elasticsearch(settings.ES_CONNECTION)
        except ElasticsearchException as e:
            raise CommandError("ERROR when connecting to Elasticsearch: %s" % e)

        # get all customers
        custs = Customer.objects.all()

        self.stdout.write("Frames in UTC:")
        # TODO feature - shared agents among customers
        # TODO optimization only enabled - deployed tests
        try:
            for cust in custs:
                for env in cust.environments.all():
                    for app in cust.apps.all():
                        for test in app.tests.all():
                            for agent in cust.agents.all():
                                for frame in frames:


                                    self.stdout.write("\t%s" % frame.isoformat())
                                    # TODO move patterns to settings
                                    index_name_date = frame.strftime("%Y.%m.%d")
                                    index_name = "uxm-data-%s" % index_name_date
                                    case_id = "%s^%s^%s^%s" % (cust.name, env.name, app.name, test.name)
                                    frame_id = "%s-%s-%s" % (agent.hostname, case_id, int(frame.timestamp()))
                                    self.stdout.write("\t\tSearching frame %s %s" % (index_name, frame_id))

                                    frame_exists = es.exists(index=index_name, doc_type="sakuli-frame", id=frame_id)

                                    if frame_exists:
                                        self.stdout.write("\t\t\tFrame found - noop")
                                    else:

                                        self.stdout.write("\t\t\tCreating frame %s %s" % (index_name, frame_id))
                                        frame_body = {
                                            "index_type": "sakuli-frame",
                                            "index_name_date": index_name_date,
                                            "index_name": index_name,
                                            "host": agent.hostname,
                                            "case_id": case_id,
                                            # TODO rename - remove case
                                            "case_frame_start": frame,
                                            "case_frame_start_seconds": frame.timestamp(),
                                            "frame_maintenance": False,
                                            "frame_state": "unknown",
                                        }
                                        out = es.index(index=index_name, doc_type="sakuli-frame", id=frame_id, body=frame_body)
                                        self.stdout.write("\t\t%s" % out)


        except ElasticsearchException as e:
            raise CommandError("ERROR when getting data from Elasticsearch: %s" % e)
