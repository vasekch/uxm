from datetime import datetime, timezone
from dateutil import parser
from math import floor

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import ElasticsearchException

from django.core.management.base import BaseCommand, CommandError

from cp.models import Customer, Environment, App

from django.conf import settings

class Command(BaseCommand):
    args = '<on|off> <customer> <env> <app> <maintenance_start> <maintenance_end>'
    help = 'Set/unset maintenance flag on SLA frames for given customer, env and app over all agents for given time period (inclusive). Example usage: "manage.py uxm_maintenance on comp01 env01 app01 2014-12-16T10:30:00+0100 2014-12-16T10:33:00+0100" or to switch off "manage.py uxm_maintenance off comp01 env01 app01 2014-12-15T12:09:00+0100 2014-12-15T12:13:00+0100"'

    def handle(self, *args, **options):

        # parse arguments
        if len(args) != 6:
            raise CommandError('Please provide correct number of arguments')

        arg_flag = args[0]
        # parse on/off flag
        if arg_flag == "on":
            flag = True
        elif arg_flag == "off":
            flag = False
        else:
            raise CommandError('First argument must be one of "on" or "off"')

        arg_cust = args[1]
        # get customer obj
        try:
            cust = Customer.objects.get(name=arg_cust)
        except Exception as e:
            raise CommandError("Arg customer not valid: %s" % e)
        # get all cust agents
        agents = cust.agents.all()
        if len(agents) == 0:
            raise CommandError("Given customer does not have any agents to put on maintenance")

        arg_env = args[2]
        # get env obj
        try:
            env = Environment.objects.get(name=arg_env)
        except Exception as e:
            raise CommandError("Arg environment not valid: %s" % e)

        arg_app = args[3]
        # get app obj
        try:
            app = App.objects.get(name=arg_app)
        except Exception as e:
            raise CommandError("Arg application not valid: %s" % e)
        # get all app tests
        # TODO think about considerating AppSetup, TestSetup and DeployedTest
        # TODO consider influence of enabled?
        # resp. think about separate maintenance index and actual frame maintenance processing after creation of an event (even unknown)
        tests = app.tests.all()
        if len(tests) == 0:
            raise CommandError("Given app does not have any test to put on maintenance")

        arg_start = args[4]
        # parse maintenance_start timestamp
        try:
            maintenance_start = parser.parse(arg_start).timestamp()
        except ValueError as e:
            raise CommandError("Arg maintenance_start is not valid timestamp %s" % e)

        arg_end = args[5]
        # parse maintenance_end timestamp
        try:
            maintenance_end = parser.parse(arg_end).timestamp()
        except ValueError as e:
            raise CommandError("Arg maintenance_end is not valid timestamp %s" % e)

        if maintenance_start >= maintenance_end:
            raise CommandError("Maintenance end must be later than maintenance start")

        # variables flag, cust, env, app, maintenance_start, maintenance_end ready
        self.stdout.write("args parsed ok: flag=%s cust=%s env=%s app=%s maintenance_start=%s maintenance_end=%s agents=%s tests=%s" % (flag, cust.name, env.name, app.name, maintenance_start, maintenance_end, agents, tests))

        frames = list()
        time_pointer = maintenance_start

        while time_pointer <= maintenance_end:
            ts = floor(time_pointer / settings.UXM_FRAME_SIZE) * settings.UXM_FRAME_SIZE
            frames.append(datetime.fromtimestamp(ts))
            time_pointer = time_pointer + settings.UXM_FRAME_SIZE

        self.stdout.write("time frames ok: total %s" % len(frames))

        # create connection to ES
        try:
            es = Elasticsearch(settings.ES_CONNECTION)
        except ElasticsearchException as e:
            raise CommandError("ERROR when connecting to Elasticsearch: %s" % e)


        # for all agents of a customer
        for agent in agents:
            for frame in frames:
                for test in tests:
                    try:
                        # check if frame exists
                        # TODO move patterns to settings - DRY to an utils.py or something
                        index_name_date = frame.strftime("%Y.%m.%d")
                        index_name = "uxm-data-%s" % index_name_date
                        case_id = "%s^%s^%s^%s" % (cust.name, env.name, app.name, test.name)
                        frame_id = "%s-%s-%s" % (agent.hostname, case_id, int(frame.timestamp()))
                        self.stdout.write("\t\tSearching frame %s %s" % (index_name, frame_id))
                        frame_exists = es.exists(index=index_name, doc_type="sakuli-frame", id=frame_id)

                        if frame_exists:
                            # update current item to maitenance
                            self.stdout.write("\t\t\tUpdating frame %s %s" % (index_name, frame_id))
                            # exit(1)
                            frame_body = {
                                "doc": {
                                    "frame_maintenance": flag
                                }
                            }
                            out = es.update(index=index_name, doc_type="sakuli-frame", id=frame_id, body=frame_body)
                            self.stdout.write("\t\t%s" % out)

                        else:
                            # create frame and set state unknown and maintenance
                            self.stdout.write("\t\t\tCreating frame %s %s" % (index_name, frame_id))
                            # exit(1)
                            frame_body = {
                                "index_type": "sakuli-frame",
                                "index_name_date": index_name_date,
                                "index_name": index_name,
                                "host": agent.hostname,
                                "case_id": case_id,
                                # TODO rename - remove case
                                "case_frame_start": frame,
                                "case_frame_start_seconds": frame.timestamp(),
                                "frame_state": "unknown",
                                "frame_maintenance": flag
                            }
                            out = es.index(index=index_name, doc_type="sakuli-frame", id=frame_id, body=frame_body)
                            self.stdout.write("\t\t%s" % out)

                    except ElasticsearchException as e:
                        raise CommandError("ERROR when getting data from Elasticsearch: %s" % e)
