
from django.forms import ValidationError
from base.forms import BaseModelForm

################################################################################

class TestSetupAdminForm(BaseModelForm):

    def clean(self):
        if self.cleaned_data["test"].app.pk != self.cleaned_data["appsetup"].app.pk:
            raise ValidationError(
                "Test and AppSetup is not compatible!",
                code="invalid"
            )
        return super(TestSetupAdminForm, self).clean()

################################################################################

class AppSetupAdminForm(BaseModelForm):

    def clean(self):
        if self.instance:
            if self.instance.testsetups.exclude(test__app=self.cleaned_data["app"]).exists():
                raise ValidationError(
                    "Exist TestSetup related to this AppSetup which is using another application (%(app)s)!",
                    params={"app": self.instance.app},
                    code="invalid"
                )
        return super(AppSetupAdminForm, self).clean()
