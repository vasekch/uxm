# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cp', '0003_auto_20141217_1128'),
    ]

    operations = [
        migrations.AddField(
            model_name='app',
            name='start_url',
            field=models.URLField(blank=True, verbose_name='Start URL', help_text='Leave empty for desktop tests', default=''),
            preserve_default=True,
        ),
    ]
