# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cp', '0005_agent_encription_interface'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='agent',
            name='encription_interface',
        ),
        migrations.AddField(
            model_name='agent',
            name='encryption_interface',
            field=models.CharField(help_text='unix format, default empty = "eth0", no need to change on fresh Win instalations, serves as computing backend for password encryption', blank='true', default='', max_length=255),
            preserve_default=True,
        ),
    ]
