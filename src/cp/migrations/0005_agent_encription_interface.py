# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cp', '0004_app_start_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='agent',
            name='encription_interface',
            field=models.CharField(default='', max_length=255, help_text='unix format, default empty = "eth0", no need to change on fresh Win instalations', blank='true'),
            preserve_default=True,
        ),
    ]
