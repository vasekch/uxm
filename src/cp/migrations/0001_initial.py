# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
import base.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Agent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('dt_created', models.DateTimeField(verbose_name='Date created', auto_now_add=True)),
                ('dt_changed', models.DateTimeField(auto_now=True, verbose_name='Date changed')),
                ('is_deleted', models.BooleanField(verbose_name='Is deleted', default=False)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('ip', models.IPAddressField()),
                ('user_name', models.CharField(max_length=255)),
                ('uxm_path', models.CharField(max_length=255)),
                ('platform', models.CharField(max_length=255, choices=[('ubuntu_1404', 'Ubuntu 14.04'), ('win7_32', 'Windows7 32bit'), ('win7_64', 'Windows7 64bit'), ('win8_32', 'Windows8 32bit'), ('win8_64', 'Windows8 64bit'), ('win10_64', 'Windows10 64bit')], default='win7_64')),
                ('position', base.fields.PositionField(verbose_name='Position', default=-1)),
                ('description', models.CharField(null=True, blank=True, max_length=255)),
                ('lat', models.DecimalField(null=True, blank=True, decimal_places=7, max_digits=10)),
                ('lon', models.DecimalField(null=True, blank=True, decimal_places=7, max_digits=10)),
                ('country_code', models.CharField(null=True, blank=True, max_length=64, help_text='ISO 3166-1')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='App',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('dt_created', models.DateTimeField(verbose_name='Date created', auto_now_add=True)),
                ('dt_changed', models.DateTimeField(auto_now=True, verbose_name='Date changed')),
                ('is_deleted', models.BooleanField(verbose_name='Is deleted', default=False)),
                ('name', models.CharField(max_length=255, help_text='Name and version', verbose_name='Name')),
                ('variables', jsonfield.fields.JSONField(blank=True, verbose_name='Variables')),
                ('is_enabled', models.BooleanField(verbose_name='Enabled', default=True)),
            ],
            options={
                'ordering': ('customer', 'is_deleted'),
                'verbose_name_plural': 'Applications',
                'verbose_name': 'Application',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AppSetup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('dt_created', models.DateTimeField(verbose_name='Date created', auto_now_add=True)),
                ('dt_changed', models.DateTimeField(auto_now=True, verbose_name='Date changed')),
                ('is_deleted', models.BooleanField(verbose_name='Is deleted', default=False)),
                ('position', base.fields.PositionField(verbose_name='Position', default=-1)),
                ('variables', jsonfield.fields.JSONField(blank=True, verbose_name='Variables')),
                ('is_enabled', models.BooleanField(verbose_name='Enabled', default=True)),
                ('app', models.ForeignKey(to='cp.App', verbose_name='Application', related_name='appsetups')),
            ],
            options={
                'ordering': ('environment', 'is_deleted', 'position'),
                'verbose_name_plural': 'Applications setup',
                'verbose_name': 'Application setup',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('dt_created', models.DateTimeField(verbose_name='Date created', auto_now_add=True)),
                ('dt_changed', models.DateTimeField(auto_now=True, verbose_name='Date changed')),
                ('is_deleted', models.BooleanField(verbose_name='Is deleted', default=False)),
                ('name', models.CharField(max_length=255, unique=True, verbose_name='Name')),
                ('position', base.fields.PositionField(verbose_name='Position', default=-1)),
                ('ico_filename', models.CharField(null=True, blank=True, max_length=255, help_text='will be searched in /media/ico/', verbose_name='Icon filename')),
                ('dashboard_url', models.CharField(null=True, blank=True, max_length=255)),
            ],
            options={
                'ordering': ('is_deleted', 'position'),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DeployedTest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('dt_created', models.DateTimeField(verbose_name='Date created', auto_now_add=True)),
                ('dt_changed', models.DateTimeField(auto_now=True, verbose_name='Date changed')),
                ('is_deleted', models.BooleanField(verbose_name='Is deleted', default=False)),
                ('threshold_warn', models.IntegerField(null=True, blank=True, verbose_name='WARN')),
                ('threshold_crit', models.IntegerField(null=True, blank=True, verbose_name='CRIT')),
                ('is_enabled', models.BooleanField(verbose_name='Enabled', default=True)),
                ('agent', models.ForeignKey(to='cp.Agent', verbose_name='Agent', related_name='deployedtests')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Environment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('dt_created', models.DateTimeField(verbose_name='Date created', auto_now_add=True)),
                ('dt_changed', models.DateTimeField(auto_now=True, verbose_name='Date changed')),
                ('is_deleted', models.BooleanField(verbose_name='Is deleted', default=False)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('position', base.fields.PositionField(verbose_name='Position', default=-1)),
                ('customer', models.ForeignKey(to='cp.Customer', verbose_name='Customer', related_name='environments')),
            ],
            options={
                'ordering': ('customer', 'is_deleted', 'position'),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('dt_created', models.DateTimeField(verbose_name='Date created', auto_now_add=True)),
                ('dt_changed', models.DateTimeField(auto_now=True, verbose_name='Date changed')),
                ('is_deleted', models.BooleanField(verbose_name='Is deleted', default=False)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('threshold_warn', models.IntegerField(null=True, blank=True, verbose_name='WARN')),
                ('threshold_crit', models.IntegerField(null=True, blank=True, verbose_name='CRIT')),
                ('is_uploaded', models.BooleanField(default=False)),
                ('app', models.ForeignKey(to='cp.App', verbose_name='Application', related_name='tests')),
            ],
            options={
                'ordering': ('is_deleted', 'name'),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TestSetup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('dt_created', models.DateTimeField(verbose_name='Date created', auto_now_add=True)),
                ('dt_changed', models.DateTimeField(auto_now=True, verbose_name='Date changed')),
                ('is_deleted', models.BooleanField(verbose_name='Is deleted', default=False)),
                ('threshold_warn', models.IntegerField(null=True, blank=True, verbose_name='WARN')),
                ('threshold_crit', models.IntegerField(null=True, blank=True, verbose_name='CRIT')),
                ('is_enabled', models.BooleanField(verbose_name='Enabled', default=True)),
                ('build_fingerprint', models.CharField(null=True, blank=True, max_length=255)),
                ('build_dt', models.DateTimeField(null=True, blank=True)),
                ('appsetup', models.ForeignKey(to='cp.AppSetup', verbose_name='AppSetup', related_name='testsetups')),
                ('test', models.ForeignKey(to='cp.Test', verbose_name='Test', related_name='testsetups')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='test',
            name='appsetups',
            field=models.ManyToManyField(through='cp.TestSetup', to='cp.AppSetup'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='environment',
            unique_together=set([('customer', 'name')]),
        ),
        migrations.AddField(
            model_name='deployedtest',
            name='testsetup',
            field=models.ForeignKey(to='cp.TestSetup', verbose_name='TestSetup', related_name='deployedtests'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='deployedtest',
            unique_together=set([('testsetup', 'agent')]),
        ),
        migrations.AddField(
            model_name='appsetup',
            name='environment',
            field=models.ForeignKey(to='cp.Environment', verbose_name='Environment', related_name='apps'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='appsetup',
            unique_together=set([('environment', 'app')]),
        ),
        migrations.AddField(
            model_name='app',
            name='customer',
            field=models.ForeignKey(to='cp.Customer', verbose_name='Customer', related_name='apps'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='app',
            unique_together=set([('customer', 'name')]),
        ),
        migrations.AddField(
            model_name='agent',
            name='customer',
            field=models.ForeignKey(to='cp.Customer', verbose_name='Customer', related_name='agents'),
            preserve_default=True,
        ),
    ]
