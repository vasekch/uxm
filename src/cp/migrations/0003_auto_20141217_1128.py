# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cp', '0002_agent_hostname'),
    ]

    operations = [
        migrations.AddField(
            model_name='agent',
            name='port',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='agent',
            name='uxm_path',
            field=models.CharField(help_text="unix format, use /cygdrive/c/uxm for Windows OS, don't use trailing slash!", max_length=255),
            preserve_default=True,
        ),
    ]
