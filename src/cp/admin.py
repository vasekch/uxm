
import os
import shutil

from django.contrib import admin, messages
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render

from base.admin import BaseAdmin, BaseInlineAdmin
from base.utils import md5_for_directory

from .models import App, Customer, Environment, AppSetup, Agent, Test, TestSetup, DeployedTest
from . import errors
from .forms import AppSetupAdminForm, TestSetupAdminForm

from django.conf import settings

################################################################################
################################################################################

# Globally disable delete selected
admin.site.disable_action('delete_selected')


class AppSetupInline(BaseInlineAdmin):
    readonly_fields = (
        "get_admin_link",
        "get_testsetups_count",
        "variables",
        "is_enabled",
    )
    fields = readonly_fields + ("is_deleted",)
    model = AppSetup


class TestSetupInline(BaseInlineAdmin):
    readonly_fields = (
        "get_admin_link",
        "appsetup",
        "test",
        "get_deployedtests_count",
        "threshold_warn",
        "threshold_crit",
        "is_enabled",
    )
    fields = readonly_fields + ("is_deleted",)
    model = TestSetup


class DeployedTestInline(BaseInlineAdmin):
    readonly_fields = (
        "get_admin_link",
        "agent",
        "testsetup",
        "threshold_crit",
        "threshold_warn",
        "is_enabled",
    )
    fields = readonly_fields + ("is_deleted",)
    model = DeployedTest


class AgentInline(BaseInlineAdmin):
    readonly_fields = (
        "get_admin_link",
        "get_deployedtests_count",
        "ip",
        "user_name",
        "uxm_path",
        "platform",
    )
    fields = readonly_fields + ("is_deleted",)
    model = Agent

class EnvironmentInline(BaseInlineAdmin):
    readonly_fields = (
        "get_admin_link",
        "name",
        "get_apps_count",
    )
    fields = readonly_fields + ("is_deleted",)
    model = Environment

################################################################################

class CustomerAdmin(BaseAdmin):
    # exclude = ("is_deleted",) # not working, because get_form method overrides it
    list_display = (
        "name",
        "admin_ico_img",
        "get_agents_count",
        "get_environments_count",
        "admin_dashboard_link",
        # "admin_is_active",
        "position",
        "is_deleted",
    )
    list_display_links = (
        "name",
        "admin_ico_img",
    )

    list_editable = ("is_deleted", )
    list_filter = ("is_deleted", )

    inlines = (
        AgentInline,
        EnvironmentInline,
    )

    def get_form(self, request, obj=None, **kwargs):
        # Proper kwargs are form, fields, exclude, formfield_callback
        if not obj: # obj is not None, so this is a change page
            kwargs.setdefault('exclude', []).append("position")
            kwargs.setdefault('exclude', []).append("is_deleted")
        else:
            kwargs.setdefault('exclude', []).append("is_deleted")
        return super(CustomerAdmin, self).get_form(request, obj, **kwargs)

    def admin_dashboard_link(self, obj):
        if obj.dashboard_url:
            return "<a href='%(url)s'>%(url)s</a>" % {"url": obj.dashboard_url}
        return "None"
    admin_dashboard_link.short_description = "Dashboard URL"
    admin_dashboard_link.admin_order_field = "dashboard_url"
    admin_dashboard_link.allow_tags = True

    def admin_ico_img(self, obj):
        if obj.ico_filename:
            return "<img src='%(url)s' />" % {
                "url": "".join([settings.ICO_URL, obj.ico_filename])
            }
        return "None"
    admin_ico_img.short_description = "Icon"
    admin_ico_img.admin_order_field = "ico_filename"
    admin_ico_img.allow_tags = True

################################################################################

class EnvironmentAdmin(BaseAdmin):
    # exclude = ("is_deleted", ) # not working, because get_form method overrides it
    list_display = (
        "name",
        "customer",
        "get_customer_name",
        "get_apps_count",
        # "admin_is_active",
        "position",
        "is_deleted",
    )
    list_display_links = (
        "name",
        "customer",
    )
    list_editable =("is_deleted",)
    list_filter = ("customer", "is_deleted", )
    search_fields = ("=name", "customer__name")
    inlines = (
        AppSetupInline,
    )

    def get_form(self, request, obj=None, **kwargs):
        # Proper kwargs are form, fields, exclude, formfield_callback
        if not obj: # obj is not None, so this is a change page
            kwargs.setdefault('exclude', []).append("position")
            kwargs.setdefault('exclude', []).append("is_deleted")
        else:
            kwargs.setdefault('exclude', []).append("is_deleted")
        return super(EnvironmentAdmin, self).get_form(request, obj, **kwargs)

    def get_customer_name(self, obj):
        return "%s" % (obj.customer.name, )
    get_customer_name.short_description = 'Customer'
    get_customer_name.admin_order_field = 'Customer__position'

################################################################################

class AgentAdmin(BaseAdmin):
    # exclude = ("is_deleted", )  # not working, because get_form method overrides it
    list_display = (
        "name",
        "customer",
        "ip",
        "hostname",
        "get_deployedtests_count",
        "admin_ip_link",
        "platform",
        "country_code",
        # "admin_is_active",
        "position",
        "is_deleted",
    )
    list_display_links = (
        "name",
        "customer",
    )
    list_editable =("is_deleted", )
    list_filter = ("customer", "is_deleted", )
    search_fields = ("name", "customer__name", )

    inlines = (
        DeployedTestInline,
    )

    def admin_ip_link(self, obj):
        if obj.ip:
            return "<a href='http://%(url)s'>%(url)s</a>" % {
                "url": obj.ip
            }
        return "None"
    admin_ip_link.short_description = "IP"
    admin_ip_link.admin_order_field = "ip"
    admin_ip_link.allow_tags = True

    def get_form(self, request, obj=None, **kwargs):
        # Proper kwargs are form, fields, exclude, formfield_callback
        if not obj: # obj is not None, so this is a change page
            # kwargs['exclude'].append('position')
            kwargs.setdefault('exclude', []).append("position")
            kwargs.setdefault('exclude', []).append("is_deleted")
        else:
            kwargs.setdefault('exclude', []).append("is_deleted")
        return super(AgentAdmin, self).get_form(request, obj, **kwargs)


################################################################################

class AppAdmin(BaseAdmin):
    # exclude = ("is_deleted", ) # not working, because get_form method overrides it
    list_display = (
        "name",
        # "get_environment",
        "is_enabled",
        # "get_testsetups_count",
        # "get_agents_count",

        # "admin_is_active",
        # "position",
        "is_deleted",
    )
    # list_display_links = (
    #     "get_environment",
    # )
    # list_editable =("is_deleted",)
    list_filter = ("is_deleted", )
    search_fields = ("=customer__name", )
    save_as = True

    # inlines = (
    #     TestSetupInline,
    # )

    # def get_environment(self, obj):
    #     return "%s" % (obj.environment.get_str(), )
    # get_environment.short_description = 'Environment'
    # get_environment.admin_order_field = 'environment__position'

    # def get_form(self, request, obj=None, **kwargs):
    #     # Proper kwargs are form, fields, exclude, formfield_callback
    #     if not obj: # obj is not None, so this is a change page
    #         kwargs.setdefault('exclude', []).append("position")
    #         kwargs.setdefault('exclude', []).append("is_deleted")
    #     else:
    #         kwargs.setdefault('exclude', []).append("is_deleted")
    #     return super(AppSetupAdmin, self).get_form(request, obj, **kwargs)

################################################################################

class AppSetupAdmin(BaseAdmin):
    # exclude = ("is_deleted", ) # not working, because get_form method overrides it
    list_display = (
        "__str__",
        "get_environment",
        "app",
        "is_enabled",
        "get_testsetups_count",
        # "get_agents_count",

        # "admin_is_active",
        "position",
        "is_deleted",
    )
    # list_display_links = (
    #     "get_environment",
    # )
    list_editable =("is_deleted",)
    list_filter = ("is_deleted", )
    search_fields = ("=environment__name", "environment__customer__name")
    save_as = True
    form = AppSetupAdminForm
    inlines = (
        TestSetupInline,
    )

    def get_environment(self, obj):
        return "%s" % (obj.environment.get_str(), )
    get_environment.short_description = 'Environment'
    get_environment.admin_order_field = 'environment__position'

    def get_form(self, request, obj=None, **kwargs):
        # Proper kwargs are form, fields, exclude, formfield_callback
        if not obj: # obj is not None, so this is a change page
            kwargs.setdefault('exclude', []).append("position")
            kwargs.setdefault('exclude', []).append("is_deleted")
        else:
            kwargs.setdefault('exclude', []).append("is_deleted")
        return super(AppSetupAdmin, self).get_form(request, obj, **kwargs)

################################################################################

class TestAdmin(BaseAdmin):
    exclude = ("is_deleted", )
    list_display = (
        "name",
        "get_testsetups_count",
        "is_uploaded",

        "admin_upload_from_dir",

        # "admin_is_active",
        "is_deleted",
    )
    readonly_fields = ("is_uploaded", )
    list_editable =("is_deleted",)
    list_filter =("is_deleted",)
    search_fields = ("name", )
    save_as = True

    inlines = (
        TestSetupInline,
    )

    #######################################
    # urls + views
    #######################################

    def get_urls(self):
        return patterns('',
            url(r'^show-inbox/(?P<test_id>\d+)/$', self.admin_site.admin_view(self.show_inbox), name="show_inbox"),
            url(r'^upload-from-inbox/(?P<test_id>\d+)/(?P<dir_name>.+)/$', self.admin_site.admin_view(self.upload_from_inbox), name="upload_from_inbox"),
        ) + super(TestAdmin, self).get_urls()

    def show_inbox(self, request, test_id):

        test = get_object_or_404(Test, pk=test_id)

        for (xxx, dirs, xxx) in os.walk(os.path.join(settings.MEDIA_ROOT, "inbox")):
            break
        to_tmpl = {
            "test": test,
            "dirs": sorted(dirs)
        }
        return render(request, "admin/show_inbox.html", to_tmpl)

    def upload_from_inbox(self, request, test_id, dir_name):
        # TODO MK pridat komentare hlavne nakonci
        test = get_object_or_404(Test, pk=test_id)

        source = os.path.join(settings.MEDIA_ROOT, "inbox", dir_name)
        target = test.get_test_source_directory_path()

        if not os.path.exists(source):
            messages.add_message(request, messages.ERROR, 'Directory "%s" not found.' % dir_name)
        elif os.path.exists(target):
            messages.add_message(request, messages.WARNING, 'Test already exists.')
        else:
            try:
                Test.validate_data(source)
            except (errors.InvalidTestError) as e:
                messages.add_message(request, messages.ERROR, 'Test is invalid: %s' % e)
            else:
                shutil.move(source, target)
                md5_for_directory(target)
                test.is_uploaded = True
                test.save()

        return HttpResponseRedirect(reverse("admin:show_inbox", args=[test.pk]))

    #######################################

    def admin_upload_from_dir(self, obj):
        return "<a href='%s'>show_inbox</a>" % reverse("admin:show_inbox", args=[obj.pk])
    admin_upload_from_dir.allow_tags = True
    admin_upload_from_dir.short_description = "Upload from dir"


################################################################################

class TestSetupAdmin(BaseAdmin):
    exclude = ("is_deleted", )
    list_display = (
        "id",
        "appsetup",
        "test",
        "get_deployedtests_count",
        "threshold_warn",
        "threshold_crit",
        "is_enabled",
        "is_deleted",
    )
    list_editable = ("is_deleted",)
    list_filter = ("is_deleted",)
    search_fields = (
        "appsetup__name",
        "test__name",
        "appsetup__environment__name",
        "appsetup__environment__customer__name",
    )

    inlines = (
        DeployedTestInline,
    )
    form = TestSetupAdminForm

class DeployedTestAdmin(BaseAdmin):
    list_display = (
        "id",
        "agent",
        "testsetup",
        "threshold_crit",
        "threshold_warn",
        "is_enabled",
        )

################################################################################

admin.site.register(Customer, CustomerAdmin)
admin.site.register(Environment, EnvironmentAdmin)
admin.site.register(App, AppAdmin)
admin.site.register(AppSetup, AppSetupAdmin)
admin.site.register(Agent, AgentAdmin)
admin.site.register(Test, TestAdmin)
admin.site.register(TestSetup, TestSetupAdmin)
admin.site.register(DeployedTest, DeployedTestAdmin)
