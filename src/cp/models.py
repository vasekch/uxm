
import os

from django.db import models
from jsonfield import JSONField

from base.fields import PositionField
from base.models import BaseModel
from . import errors

from django.conf import settings

################################################################################
################################################################################


class Customer(BaseModel):
    name = models.CharField(max_length=255, verbose_name='Name', unique=True)
    position = PositionField(verbose_name='Position')
    ico_filename = models.CharField(max_length=255, verbose_name='Icon filename', blank=True, null=True, help_text="will be searched in %s" % settings.ICO_URL)
    dashboard_url = models.CharField(max_length=255, blank=True, null=True)

    def get_str(self):
        return self.name

    class Meta:
        ordering = ("is_deleted", "position", )

    def get_agents_count(self, deleted=False):
        return self.agents.filter(is_deleted=deleted).count()
    get_agents_count.short_description = "Agents"

    def get_environments_count(self, deleted=False):
        return self.environments.filter(is_deleted=deleted).count()
    get_environments_count.short_description = "Environments"


################################################################################


class Environment(BaseModel):
    name = models.CharField(max_length=255, verbose_name='Name')
    customer = models.ForeignKey(Customer, related_name='environments', verbose_name='Customer')
    position = PositionField(verbose_name='Position', collection='customer')

    class Meta:
        unique_together = (("customer", "name"),)
        ordering = ("customer", "is_deleted", "position",)

    def get_str(self):
        return "%s - %s" % (self.customer.name, self.name)

    def get_apps_count(self, deleted=False):
        # return 4
        return self.apps.filter(is_deleted=deleted).count()
    get_apps_count.short_description = "Applications"


################################################################################


class App(BaseModel):
    customer = models.ForeignKey(Customer, related_name='apps', verbose_name='Customer')
    name = models.CharField(max_length=255, verbose_name='Name', help_text='Name and version')
    start_url = models.URLField(blank=True, default='', verbose_name='Start URL', help_text='Leave empty for desktop tests')
    variables = JSONField(verbose_name='Variables', blank=True)
    is_enabled = models.BooleanField(verbose_name='Enabled', default=True)

    class Meta:
        unique_together = (("customer", "name"),)
        ordering = ("customer", "is_deleted", )
        verbose_name = "Application"
        verbose_name_plural = "Applications"

    def get_str(self):
        return self.name

################################################################################

class AppSetup(BaseModel):
    environment = models.ForeignKey(Environment, related_name='apps', verbose_name='Environment')
    app = models.ForeignKey(App, related_name='appsetups', verbose_name='Application')
    position = PositionField(verbose_name='Position', collection='environment')
    variables = JSONField(verbose_name='Variables', blank=True)
    is_enabled = models.BooleanField(verbose_name='Enabled', default=True)

    class Meta:
        unique_together = (("environment", "app"),)
        ordering = ("environment", "is_deleted", "position", )
        verbose_name = "Application setup"
        verbose_name_plural = "Applications setup"

    def get_str(self):
        return "%s: %s" % (self.environment.name, self.app.name)

    def get_variables(self):
        # TODO maybe join together ??
        return self.variables or self.app.variables

    def get_testsetups_count(self, deleted=False):
        return self.testsetups.filter(is_deleted=deleted).count()
    get_testsetups_count.short_description = "Test Setups"


################################################################################


class Agent(BaseModel):
    name = models.CharField(max_length=255, verbose_name='Name')
    ip = models.IPAddressField()
    port = models.IntegerField(blank=True, null=True)
    hostname = models.CharField(max_length=255)
    user_name = models.CharField(max_length=255)
    uxm_path = models.CharField(max_length=255, help_text='unix format, use /cygdrive/c/uxm for Windows OS, don\'t use trailing slash!')
    platform = models.CharField(choices=settings.PLATFORM_CHOICES, default=settings.PLATFORM_DEFAULT, max_length=255)
    encryption_interface = models.CharField(max_length=255, blank="true", default="", help_text='unix format, default empty = "eth0", no need to change on fresh Win instalations, serves as computing backend for password encryption')

    customer = models.ForeignKey(Customer, related_name='agents', verbose_name='Customer')
    position = PositionField(verbose_name='Position', collection='customer')
    description = models.CharField(max_length=255, blank=True, null=True)

    lat = models.DecimalField(max_digits=10, decimal_places=7, blank=True, null=True)
    lon = models.DecimalField(max_digits=10, decimal_places=7, blank=True, null=True)
    country_code = models.CharField(max_length=64, help_text='ISO 3166-1', blank=True, null=True)

    def get_str(self):
        return self.name

    def get_deployedtests_count(self, deleted=False):
        return self.deployedtests.filter(is_deleted=deleted).count()
    get_deployedtests_count.short_description = "Deployed Tests"

    def get_outbox_agent_path(self):
        return os.path.abspath(os.path.join(
            settings.MEDIA_ROOT,
            "outbox",
            "agent_%s" % str(self.pk).zfill(6)
        ))

    def get_log_file_name(self):
        return os.path.abspath(os.path.join(
            settings.DEPLOY_TEST_PATH, "agent_%s.log" % self.pk
        ))

    def get_os_path(self):
        if self.platform in settings.PLATFORM_WINDOWS:
            return self.uxm_path.replace("/cygdrive/c", "c:").replace("/", "\\")
        else:
            return self.uxm_path

    def get_destination_uri(self):
        return "%(user_name)s@%(ip)s:%(uxm_path)s" % self.__dict__


################################################################################


class Test(BaseModel):
    app = models.ForeignKey(App, verbose_name='Application', related_name='tests')
    name = models.CharField(max_length=255, verbose_name='Name')
    threshold_warn = models.IntegerField(verbose_name='WARN', blank=True, null=True)
    threshold_crit = models.IntegerField(verbose_name='CRIT', blank=True, null=True)
    is_uploaded = models.BooleanField(default=False)
    appsetups = models.ManyToManyField(AppSetup, through='TestSetup',
        # TODO: through_fields
        )

    TEST_SCRIPT_NAME = "test.js"

    #######################################

    class Meta:
        ordering = ("is_deleted", "name", )

    @classmethod
    def validate_data(cls, path):
        """otestuje/zvaliduje adresar s testem"""

        if cls.TEST_SCRIPT_NAME not in os.listdir(path):
            raise errors.InvalidTestError("Selected directory does not contain file %s" %  cls.TEST_SCRIPT_NAME)

        # TODO others checks

        return True

    def get_str(self):
        return self.name

    def get_testsetups_count(self, deleted=False):
        return self.testsetups.filter(is_deleted=deleted).count()
    get_testsetups_count.short_description = "Test Setups"

    def get_test_source_directory_name(self):
        return "test_id_%s" % str(self.pk).zfill(6)

    def get_test_source_directory_path(self):
        return os.path.join(
            settings.MEDIA_ROOT,
            "source",
            self.get_test_source_directory_name()
        )

    def upload_data(self, path):
        """presune a prejmenuje adresar s testem"""
        pass


################################################################################


class TestSetup(BaseModel):
    test = models.ForeignKey(Test, related_name='testsetups', verbose_name='Test')
    appsetup = models.ForeignKey(AppSetup, related_name='testsetups', verbose_name='AppSetup')
    threshold_warn = models.IntegerField(verbose_name='WARN', blank=True, null=True)
    threshold_crit = models.IntegerField(verbose_name='CRIT', blank=True, null=True)
    is_enabled = models.BooleanField(verbose_name='Enabled', default=True)

    build_fingerprint = models.CharField(max_length=255, null=True, blank=True)
    build_dt = models.DateTimeField(null=True, blank=True)

    def get_str(self):
        return "%s - %s - %s" % (
            self.appsetup.environment,
            self.appsetup,
            self.test
        )

    def is_builded(self):
        return os.path.exists(self.get_build_directory_path())

    def get_deployedtests_count(self, deleted=False):
        return self.deployedtests.filter(is_deleted=deleted).count()
    get_deployedtests_count.short_description = "Deployed Tests"

    def get_build_directory_name(self):
        # return "test_setup_%s" % str(self.pk).zfill(6)
        # due to logstash scope recognition
        return "%s^%s^%s^%s" % (self.appsetup.environment.customer.name, self.appsetup.environment.name, self.appsetup.app.name, self.test.name)

    def get_build_directory_path(self):
        return os.path.join(
            settings.MEDIA_ROOT,
            "build",
            self.get_build_directory_name()
        )


################################################################################


class DeployedTest(BaseModel):
    agent = models.ForeignKey(Agent, related_name='deployedtests', verbose_name='Agent')
    testsetup = models.ForeignKey(TestSetup, related_name='deployedtests', verbose_name='TestSetup')
    threshold_warn = models.IntegerField(verbose_name='WARN', blank=True, null=True)
    threshold_crit = models.IntegerField(verbose_name='CRIT', blank=True, null=True)
    is_enabled = models.BooleanField(verbose_name='Enabled', default=True)

    class Meta:
        unique_together = (
            ("testsetup", "agent")
        )

    def get_str(self):
        return "%s@%s" % (self.testsetup, self.agent)

    def get_threshold_warn(self):
        """override eval."""
        pass

    def get_threshold_crit(self):
        """override eval."""
        pass
