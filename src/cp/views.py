
from datetime import datetime
import itertools
import os
import shutil

from django import http
from django.contrib import messages
from django.shortcuts import get_object_or_404, render
from django.template.loader import render_to_string, get_template
from django.template.base import VariableNode, VariableDoesNotExist
from django.views.generic import TemplateView, DetailView, View

from .models import Agent, Customer, DeployedTest, Test, TestSetup
from base.utils import md5_for_directory, CheckTemplateMixin
from base.views import JSONResponseMixin
from base.logging_utils import LoggerMixin
import tasks

from django.conf import settings

################################################################################
################################################################################


class IndexView(TemplateView):
    template_name = "dashboard.html"

    def get_context_data(self, **kwargs):
        kwargs = super(IndexView, self).get_context_data(**kwargs)
        active_customer = self.request.session.get("active_customer")
        agents = Agent.objects.filter(customer=active_customer)
        testsetups = TestSetup.objects.filter(appsetup__environment__customer=active_customer)

        matrix = {}
        for testsetup, agent in itertools.product(testsetups, agents):  # cartesian product
            try:
                deployedtest = DeployedTest.objects.get(agent=agent, testsetup=testsetup)
            except DeployedTest.DoesNotExist:
                deployedtest = None
            matrix.setdefault(testsetup.pk, {})
            matrix[testsetup.pk].setdefault(agent.pk, deployedtest)

        kwargs.update({
            "agents": agents,
            "testsetups": testsetups,
            "matrix": matrix
        })

        return kwargs

################################################################################


class SelectCustomerView(DetailView):
    model = Customer

    def get(self, request, *args, **kwargs):
        request.session["active_customer"] = self.get_object().pk
        return http.HttpResponseRedirect(request.GET.get("next") or "/")

################################################################################


class ToggleDeployedTestEnabledView(DetailView):
    model = DeployedTest

    def get(self, request, *args, **kwargs):
        td = self.get_object()
        td.is_enabled = not td.is_enabled
        td.save()
        return http.HttpResponseRedirect(request.GET.get("next") or "/")

################################################################################


class ToggleTestSetupEnabledView(DetailView):
    model = TestSetup

    def get(self, request, *args, **kwargs):
        td = self.get_object()
        td.is_enabled = not td.is_enabled
        td.save()
        return http.HttpResponseRedirect(request.GET.get("next") or "/")

################################################################################


class BuildTestSetupView(DetailView, LoggerMixin, CheckTemplateMixin):
    """make copy from source to build, fill template and create md5"""
    model = TestSetup

    def get(self, request, *args, **kwargs):
        logger = self.get_logger()
        ret = http.HttpResponseRedirect(request.GET.get("next") or "/")

        ts = self.get_object()
        source = ts.test.get_test_source_directory_path()
        target = ts.get_build_directory_path()

        if not os.path.exists(os.path.join(source, Test.TEST_SCRIPT_NAME)):
            messages.add_message(request, messages.WARNING, "Source destination does not exists!")
            return ret

        if os.path.exists(target):
            messages.add_message(request, messages.WARNING, "Target destination exists!")
            return ret

        #  check missing variables
        missing_variables = self.get_missing_variables(
            source,
            Test.TEST_SCRIPT_NAME,
            set(
                ts.appsetup.get_variables().keys()
                if isinstance(ts.appsetup.get_variables(), dict)
                else []
            ))
        if missing_variables:
            messages.add_message(
                request,
                messages.WARNING,
                'Some variables (%s) missing in App variables.' % ", ".join(sorted(missing_variables))
                )
            return ret

        # copy test
        shutil.copytree(source, target)

        # fill test with variables
        try:
            with open(os.path.join(target, Test.TEST_SCRIPT_NAME), "w") as script_file:
                script_file.write(render_to_string(
                    Test.TEST_SCRIPT_NAME,
                    ts.appsetup.get_variables(),
                    dirs=[source]
                ))

            ts.build_fingerprint = md5_for_directory(target)
            ts.build_dt = datetime.now()
            ts.save()
        except Exception as e:
            logger.error("Build TestSetup fail: %s" % {
                "error": e,
                "test_setup": ts.pk,
            })
            messages.add_message(request, messages.ERROR, 'Error: %s' % e)
            shutil.rmtree(target)
        else:
            logger.info("Build TestSetup complete: %s" % {
                "test_setup": ts.pk,
                "path": target
            })
            messages.add_message(request, messages.SUCCESS, 'Success. Test built.')

        return ret

################################################################################


class GenerateDeployedTestView(View, LoggerMixin, CheckTemplateMixin):
    """
        Prepare testsuite for deployment.
        Complete tests and add some conf files.
    """

    def get(self, request, agent_pk, *args, **kwargs):
        self.logger = self.get_logger()
        self.agent = get_object_or_404(Agent, pk=agent_pk)
        testsetups = TestSetup.objects.filter(
            is_enabled=True,
            is_deleted=False,
            appsetup__environment__customer=self.agent.customer
        )

        self.outbox_path = self.agent.get_outbox_agent_path()
        self._create_structure()

        # create DeployedTest if not exists
        for testsetup in testsetups:
            DeployedTest.objects.get_or_create(
                agent=self.agent,
                testsetup=testsetup
            )

        deployedtests = DeployedTest.objects.filter(
            testsetup__in=testsetups,
            agent=self.agent,
            is_enabled=True
        )

        # try to use phantomjs as hidden web browser
        desktop_only = True

        self.available_deployed_tests = []
        for deployedtest in deployedtests.iterator():
            build_directory_path = deployedtest.testsetup.get_build_directory_path()
            start_url = deployedtest.testsetup.appsetup.app.start_url

            if not os.path.exists(build_directory_path):
                continue  # test is not builded

            self.available_deployed_tests.append(deployedtest)
            build_directory_name = deployedtest.testsetup.get_build_directory_name()

            # copy build to agent's output
            shutil.copytree(
                build_directory_path,
                os.path.join(self.outbox_path, "testsuite", build_directory_name)
            )

            # 'register' test
            self._add_to_testsuite("%s %s" % (os.path.join(build_directory_name, Test.TEST_SCRIPT_NAME), start_url if start_url else "http://example.com"))

            # if a single deployed test is web test, than cannot use phantomjs
            desktop_only = False if len(start_url) else desktop_only


        # TODO zobecnit
        self.fill_testsuite_properties(desktop_only)
        self.fill_procs_to_kill(desktop_only)
        self.fill_start_bat()
        self.fill_logstash_forwarder_conf()

        self.logger.info("Generated DeployedTest: %s" % {"path": self.outbox_path})
        messages.add_message(request, messages.SUCCESS, 'Success. Agent ready for deployment.')

        return http.HttpResponseRedirect(request.GET.get("next") or "/")

    #######################################

    def _add_to_testsuite(self, row):
        """
            register all tests to file testsuite.suite
        """
        testsuite_file_path = os.path.join(self.outbox_path, "testsuite", "testsuite.suite")
        with open(testsuite_file_path, "a") as testsuite_file:
            testsuite_file.write("%s\n" % row)

    #######################################

    def fill_testsuite_properties(self, desktop_only):
        context = {
            'test_id': "agent_%s" % str(self.agent.pk).zfill(6),
            'test_name': "%s" % self.agent.name,
            'threshold_warn': "%s" % (settings.UXM_FRAME_SIZE - 20),
            'threshold_crit': "%s" % (settings.UXM_FRAME_SIZE - 10),
            'browser': "phantomjs" if desktop_only else "firefox",
            'encryption_interface': "sakuli.encryption.interface=%s\n" % self.agent.encryption_interface if self.agent.encryption_interface else "",
            'encryption_testmode': "false" if self.agent.encryption_interface else "true"
        }
        self._fill_file(
            os.path.join(self.outbox_path, "testsuite"),
            "testsuite.properties",
            context
        )

    def fill_procs_to_kill(self, desktop_only):
        context = {
            'firefox': "#" if desktop_only else "",
            'phantomjs': "#" if not desktop_only else "",
        }
        self._fill_file(
            os.path.join(self.outbox_path, "conf"),
            "procs_to_kill.txt",
            context
        )

    def fill_start_bat(self):
        context = {
            'uxm_home_dir': self.agent.get_os_path(),
        }
        self._fill_file(
            self.outbox_path,
            "uxm_start.bat",
            context
        )

    def fill_logstash_forwarder_conf(self):
        context = {
            'agent_path': self.agent.get_os_path().replace("\\", "\\\\"),
            'uxm_logstash_ip': settings.UXM_LOGSTASH_IP,
            'uxm_logstash_port': settings.UXM_LOGSTASH_PORT,
        }
        self._fill_file(
            os.path.join(self.outbox_path, "conf"),
            "logstash-forwarder.conf",
            context
        )

    #######################################

    #######################################

    def _create_structure(self):
        """
            copy structure from template
        """
        if os.path.exists(self.outbox_path):
            shutil.rmtree(self.outbox_path)
            self.logger.info("Deleted directory: %s" % {"path": self.outbox_path})

        os.mkdir(self.outbox_path)

        files = [
            ["testsuite", "testsuite.properties"],
            ["testsuite", "testsuite.suite"],
            ["conf", "logstash-forwarder.conf"],
            ["conf", "procs_to_kill.txt"],
            ["conf", "uxm_server.crt"],
            ["", "uxm_start.bat"],
        ]

        # copy structure from template
        for dir_name, file_name in files:

            dir_path = os.path.join(self.outbox_path, dir_name)

            if not os.path.exists(dir_path):
                os.mkdir(dir_path)

            # copy from template
            shutil.copy2(
                os.path.join(settings.MEDIA_ROOT, "templates", dir_name, file_name),
                os.path.join(dir_path, file_name),
            )

    #######################################

    def _fill_file(self, the_dir, the_file_name, context):
        """
        general method for fill file in directory with context
        """
        the_path = os.path.join(the_dir, the_file_name)

        # check missing variables
        missing = self.get_missing_variables(
            the_dir,
            the_file_name,
            set(context.keys())
        )
        if missing:
            raise VariableDoesNotExist(
                "Missing variables [%s] for file %s." % (", ".join(missing), the_path)
            )

        # write variables to template
        _tmp_file_path = "%s_tmp" % the_path
        with open(_tmp_file_path, "w") as _file:
            _file.write(render_to_string(
                the_path,
                context,
                dirs=[the_dir]
            ))
        shutil.move(_tmp_file_path, the_path)

################################################################################


class PushDeployedTestView(View, LoggerMixin):

    def get(self, request, agent_pk, *args, **kwargs):
        agent = get_object_or_404(Agent, pk=agent_pk)

        log_file = agent.get_log_file_name()

        cmd = '''
            echo "%(start_symbol)s" > %(log_file_name)s;
            echo 'rsync -avz %(extra_params)s--del --exclude "testsuite/_logs" --exclude "*.log" %(source)s/ %(destination)s 2>&1;';
                  rsync -avz %(extra_params)s--del --exclude "testsuite/_logs" --exclude "*.log" %(source)s/ %(destination)s 2>&1;
            echo "%(done_symbol)s" >> %(log_file_name)s
        ''' % {
            "source": agent.get_outbox_agent_path(),
            "destination": agent.get_destination_uri(),
            "extra_params": "-e \"ssh -p %s\" " % agent.port if agent.port else "",
            "start_symbol": settings.PUSH_START_SYMBOL,
            "done_symbol": settings.PUSH_DONE_SYMBOL,
            "log_file_name": agent.get_log_file_name()
        }

        # run in background
        tasks.push.delay(cmd, log_file)

        context = {
            "agent": agent
        }
        return render(request, "push.html", context)


class PushDeployedTestStatusView(View, JSONResponseMixin):

    def get(self, request, agent_pk, *args, **kwargs):
        agent = get_object_or_404(Agent, pk=agent_pk)

        with open(agent.get_log_file_name(), "r") as log:
            log = log.read()
            is_push_done = bool(settings.PUSH_DONE_SYMBOL in log)

        return self.json_response({
            "agent": log,
            "push_done": is_push_done
        })

################################################################################



# class AppCloneView(FormView):
#     pass
