
from .models import Customer

def main(request):

    try:
        active_customer = Customer.objects.get(pk=request.session["active_customer"])
    except (Customer.DoesNotExist, KeyError):
        active_customer = Customer.objects.all()[:1]

    return {
        "customers": Customer.objects.iterator(),
        "active_customer": active_customer
    }
