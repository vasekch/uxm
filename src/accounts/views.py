# # -*- coding: utf-8 -*-

# from django.conf import settings
# from django.contrib.auth.forms import AuthenticationForm
# from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login, logout as auth_logout
# from django.contrib.sites.models import get_current_site
# from django.core.urlresolvers import reverse
# from django.http import HttpResponseRedirect
# from django.shortcuts import resolve_url
# from django.template.response import TemplateResponse
# from django.utils.http import is_safe_url


# def login_view(request, current_app=None, extra_context=None):

#     redirect_to = request.REQUEST.get(REDIRECT_FIELD_NAME, '')

#     if request.user.is_authenticated():
#         return HttpResponseRedirect(redirect_to or reverse("homepage"))

#     if request.method == "POST":
#         form = AuthenticationForm(data=request.POST)
#         if form.is_valid():

#             # Ensure the user-originating redirection url is safe.
#             if not is_safe_url(url=redirect_to, host=request.get_host()):
#                 redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

#             # Okay, security check complete. Log the user in.
#             auth_login(request, form.get_user())

#             if request.session.test_cookie_worked():
#                 request.session.delete_test_cookie()

#             return HttpResponseRedirect(redirect_to)
#     else:
#         form = AuthenticationForm(request)

#     request.session.set_test_cookie()

#     current_site = get_current_site(request)

#     context = {
#         'form': form,
#         REDIRECT_FIELD_NAME: redirect_to,
#         'site': current_site,
#         'site_name': current_site.name,
#     }
#     if extra_context is not None:
#         context.update(extra_context)
#     return TemplateResponse(request, "auth/login.html", context,
#                             current_app=current_app)


# def logout_view(request):
#     """
#     Logs out the user and displays 'You are logged out' message.
#     """
#     auth_logout(request)
#     return HttpResponseRedirect(reverse("homepage"))
