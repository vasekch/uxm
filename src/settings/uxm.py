
# TODO make agnet / test specific
UXM_FRAME_SIZE = 1 * 60 # 1 minute



PLATFORM_WINDOWS = ("win7_32", "win7_64", "win8_32", "win8_64", "win10_64")

PLATFORM_DEFAULT = "win7_64"
PLATFORM_CHOICES = (
    ("ubuntu_1404", "Ubuntu 14.04"),
    ("win7_32", "Windows7 32bit"),
    (PLATFORM_DEFAULT, "Windows7 64bit"),
    ("win8_32", "Windows8 32bit"),
    ("win8_64", "Windows8 64bit"),
    ("win10_64", "Windows10 64bit"),
)

PUSH_START_SYMBOL = "### START ###"
PUSH_DONE_SYMBOL = "### DONE ###"


UXM_LOGSTASH_IP = "127.0.0.1"
UXM_LOGSTASH_PORT = "5000"
