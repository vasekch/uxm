"""
Django settings for uxm project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'jsv(+ihyn1_@r-3&t^wvgtq5hkra*a&iw9ofl49kb(z&^_esuk'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    "accounts",
    "base",
    "cp",

    "djcelery",
    # 'kombu.transport.django',

    "django_extensions"
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'wsgi.application'

################################################################################
# TEMPLATES
################################################################################

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',

    "cp.context_processors.main"
)


################################################################################

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

LANGUAGES = (
    ('en', 'English'),
)

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


################################################################################
# LOGGING
################################################################################

LOG_DIR = os.path.join(BASE_DIR, "../logs")
SYSTEM_LOG = os.path.join(LOG_DIR, 'system.log')

LOGGING = {
    'version': 1,
    'formatters': {
        # 'plain': {
        #     'format': '%(asctime)s %(message)s'
        # },
        'verbose': {
            'format': '%(levelname)s:[%(asctime)s] <%(name)s|%(filename)s:%(lineno)s> %(message)s'
        },
    },
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'filters': []
        },
        'system': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': SYSTEM_LOG,
            'maxBytes': 1024 * 1024 * 5,  # 5 MB
            'backupCount': 100,
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'system': {
            'handlers': ['system'],
            'level': 'INFO',
            'propagate': False,
        },
    }
}

################################################################################

LOGIN_URL = "/login/"
LOGOUT_URL = "/logout/"

### STATIC #####################################################################

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)


MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

ICO_ROOT = os.path.join(MEDIA_ROOT, 'ico')
ICO_URL = '%sico/' % MEDIA_URL


# ################################################################################
# # CELERY
# ################################################################################

BROKER_URL = 'redis://localhost:6379/0'
CELERY_QUEUE_NAME = "uxm"
# CELERY_QUEUES = {
#     CELERY_QUEUE_NAME: {
#         "exchange": CELERY_QUEUE_NAME,
#         "binding_key": CELERY_QUEUE_NAME,
#     }
# }
CELERY_DEFAULT_QUEUE = CELERY_QUEUE_NAME
# CELERY_RESULT_BACKEND = "redis://localhost:6379/0"

# import djcelery
# djcelery.setup_loader()

################################################################################

DEPLOY_TEST_PATH = os.path.join(LOG_DIR, "deploy_test")

################################################################################

EMAIL_SUBJECT_PREFIX = '[UXM] '
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'


# ################################################################################
# # ELASTICSEARCH
# ################################################################################

# according to http://elasticsearch-py.readthedocs.org/en/master/api.html#elasticsearch
ES_CONNECTION = [
    {'host': 'localhost'},
]
