
from celery import Celery
import subprocess

import settings

app = Celery('tasks', broker=settings.BROKER_URL)

@app.task
def push(cmd, log_file_path):
    with open(log_file_path, "a") as log:
        subprocess.Popen(
            cmd,
            shell=True,
            stdin=subprocess.PIPE,
            stdout=log,
        )
