# Firefox
{{firefox}}C:\Program Files (x86)\Mozilla Firefox\firefox.exe
# IE
#C:\Program Files (x86)\Internet Explorer\iexplore.exe
# PhantomJS
{{phantomjs}}C:\sakuli\phantomjs\phantomjs.exe
#Calc
calc.exe
# Sakuli itself
java%sakuli.jar
