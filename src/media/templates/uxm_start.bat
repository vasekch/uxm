@echo off

set SUITE=testsuite
set UXM_HOME={{ uxm_home_dir }}

set TEST_SUITE_FOLDER=%UXM_HOME%\%SUITE%

set SAKULI_JARS=%SAKULI_HOME%\bin\lib\*;%SAKULI_HOME%\bin\lib\resource;

cscript.exe %SAKULI_HOME%\scripts\helper\vb_scripts\killproc.vbs -f %UXM_HOME%\conf\procs_to_kill.txt

echo jar-file: %SAKULI_JARS%
java -Duser.language=en -Duser.timezone=UTC -Dsikuli.Home=%SAKULI_HOME%\bin\lib -classpath %SAKULI_HOME%\bin\sakuli.jar;%SAKULI_JARS% de.consol.sakuli.starter.SakuliStarter -run "%TEST_SUITE_FOLDER%" "%SAKULI_HOME%\_include"

exit
