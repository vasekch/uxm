
import os
import hashlib
from functools import partial

from django.template.loader import get_template
from django.template.base import VariableNode
from django.template.defaulttags import ForNode

def md5sum(filename):
    with open(filename, mode='rb') as f:
        d = hashlib.md5()
        for buf in iter(partial(f.read, 128), b''):
            d.update(buf)
    return d.hexdigest()

def md5_for_directory(directory):
    """
        @return fingerprint of static files (css, js)
    """
    d = hashlib.md5()
    for root, sub_folders, files in os.walk(directory):
        for filename in files:
            d.update(md5sum(os.path.join(root, filename)).encode('utf-8'))
    return d.hexdigest()

class CheckTemplateMixin(object):
    def get_missing_variables(self, template_dir, template_name, available_vars):
        """
        find all missing variables in template
        """
        tmpl = get_template(template_name, dirs=[template_dir])

        template_vars = set()
        for node in tmpl.nodelist:
            if isinstance(node, VariableNode):
                template_vars.add(node.filter_expression.token)
            elif isinstance(node, ForNode):
                template_vars.add(node.sequence.token)

        return template_vars - available_vars
