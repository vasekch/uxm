
from django.template import Library

################################################################################

register = Library()

@register.assignment_tag
def get_from_dict(obj, *keys):
    ret = obj
    for key in keys:
        ret = ret.get(key)
    return ret
