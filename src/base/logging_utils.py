
import logging

class LoggerMixin(object):
    def get_logger(self):
        return logging.getLogger("system.%s" % self.__class__.__name__)

