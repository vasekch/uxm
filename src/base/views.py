# from django.conf import settings
# from django.contrib.auth.forms import AuthenticationForm
# from django.contrib.auth import login as auth_login, logout as auth_logout
# from django.http import HttpResponseRedirect
# from django.utils.decorators import method_decorator
# from django.views.decorators.debug import sensitive_post_parameters
# from django.views import FormView, View

# class Login(FormView):
#     form_class = AuthenticationForm

#     def form_valid(self, form):
#         redirect_to = settings.LOGIN_REDIRECT_URL
#         auth_login(self.request, form.get_user())
#         if self.request.session.test_cookie_worked():
#             self.request.session.delete_test_cookie()
#         return HttpResponseRedirect(redirect_to)

#     def form_invalid(self, form):
#         return self.render_to_response(self.get_context_data(form=form))

#     @method_decorator(sensitive_post_parameters('password'))
#     def dispatch(self, request, *args, **kwargs):
#         request.session.set_test_cookie()
#         return super(Login, self).dispatch(request, *args, **kwargs)

# class Logout(View):
#     def get(self, request, *args, **kwargs):
#         auth_logout(request)
#         return HttpResponseRedirect(settings.LOGOUT_REDIRECT_URL)


# class LoginRequiredMixin(object):

#     @method_decorator(login_required)
#     def dispatch(self, *args, **kwargs):
#         return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


from django import http
import json

class JSONResponseMixin(object):
    # def render_to_response(self, context):
    #     "Returns a JSON response containing 'context' as payload"
    #     return self.get_json_response(self.convert_context_to_json(context))

    def json_response(self, context, **httpresponse_kwargs):
        "Construct an `HttpResponse` object."
        return http.HttpResponse(self.convert_context_to_json(context),
                                 content_type='application/json',
                                 **httpresponse_kwargs)

    def convert_context_to_json(self, context):
        "Convert the context dictionary into a JSON object"
        # Note: This is *EXTREMELY* naive; in reality, you'll need
        # to do much more complex handling to ensure that arbitrary
        # objects -- such as Django model instances or querysets
        # -- can be serialized as JSON.
        return json.dumps(context)
