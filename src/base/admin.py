from django.contrib import admin

class BaseAdmin(admin.ModelAdmin):

    pass
    # def admin_is_active(self, obj):
    #     return not obj.is_deleted
    # admin_is_active.admin_order_field = "admin_is_active"
    # admin_is_active.short_description = "Is active"
    # admin_is_active.boolean = True


class BaseInlineAdmin(admin.TabularInline):
    extra = 0
    max_num = 0
    can_delete = False

    def get_admin_link(self, obj):
        return "<a href='%(url)s'>%(name)s</a>" % {"url": obj.get_admin_url(), "name": obj.__str__() or "[str]"}
    get_admin_link.short_description = "Admin Link"
    get_admin_link.allow_tags = True
