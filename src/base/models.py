
from django.core import urlresolvers
from django.contrib.contenttypes.models import ContentType
from django.db import models

# TODO
# class BaseManager()
#     pass
#   method for is_deleted func.


class BaseModel(models.Model):
    dt_created = models.DateTimeField(auto_now_add=True, verbose_name="Date created")
    dt_changed = models.DateTimeField(auto_now=True, verbose_name="Date changed")
    is_deleted = models.BooleanField(default=False, verbose_name='Is deleted')

    # TODO
    # objects =

    class Meta:
        abstract = True

    def __str__(self):
        return "%s%s [%s]" % (
            "DEL " if self.is_deleted else "",
            self.get_str(),
            self.id,
        )

    def get_str(self):
        raise NotImplementedError()

    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        return urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))
