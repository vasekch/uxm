#!/usr/bin/env bash

# Check if we're in a virtual environment.
VE=`echo $VIRTUAL_ENV`
if [ -z $VE ]; then
    # The virtualenv variable is null, so we are not in
    # an active virtual environment.
    echo "ERROR Not in a virtual environment"
    exit 1
fi

echo
echo "TODO check python version ? >3"

echo
echo "Virtual environment found $VIRTUAL_ENV"

echo
echo "TODO check settings local"

echo
echo "Installing requirements"
pip install -r requirements.txt

echo
echo "Available updates for consideration"
./src/manage.py pipchecker

echo
echo "migrate database to new structure"
./src/manage.py migrate
